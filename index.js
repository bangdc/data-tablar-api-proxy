var express = require('express');
var _ = require('lodash');
var https = require('https');
var querystring = require('querystring');
var server = express();
//z6YJBSjk3aTbrSohiydF5WikJexSglBd
//dSfmbVx5L5xyJHokY2yj8KPeGSSTYHh0
server.set('port', (process.env.PORT || 5002));

server.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

server.get('/', function(req, res) {
	var query = {};
	query.page_size = req.query.dttbNumRowsPerPage || 10;
	query.page = req.query.dttbPage || 1;

	try {
		var dttbFilters = JSON.parse(req.query.dttbFilters);
		_.forEach(dttbFilters, function(filter) {
			query[filter.field] = filter.value;
		});
	} catch (e) {
		console.log(e);
	}

	switch (req.query.dttbSortCol) {
		case 'trending':
			query.sort_by = 'trending';
			query.sort_direction = req.query.dttbSortDir || 'desc';
			break;

		case 'number_of_sales':
			query.sort_by = 'sales';
			query.sort_direction = req.query.dttbSortDir || 'desc';
			break;

		case 'price':
			query.sort_by = 'price';
			query.sort_direction = req.query.dttbSortDir || 'desc';
			break;

		case 'name':
			query.sort_by = 'name';
			query.sort_direction = req.query.dttbSortDir || 'desc';
			break;

		default:
			break;
	}
	console.log(querystring.stringify(query));
	var options = {
		host: 'api.envato.com',
		path: '/v1/discovery/search/search/item' + '?' + querystring.stringify(query),
		headers: {
			'Authorization': 'Bearer dSfmbVx5L5xyJHokY2yj8KPeGSSTYHh0',
		}
	};
	callback = function(response) {
		var result = '';
		//another chunk of data has been recieved, so append it to `result`
		response.on('data', function(chunk) {
			result += chunk;
		});

		//the whole response has been recieved, so we just print it out here
		response.on('end', function() {
			try {
				var parsedResult = JSON.parse(result);
				var items = [];
				_.forEach(parsedResult.matches, function(item) {
					var thumbnail_preview;
					if (item.previews.hasOwnProperty('thumbnail_preview')) {
						thumbnail_preview = item.previews.thumbnail_preview.large_url;
					} else {
						thumbnail_preview = '';
					}
					items.push({
						id: item.id,
						name: item.name,
						author_username: item.author_username,
						author_url: item.author_url + '?ref=bangdc',
						description: item.description,
						number_of_sales: item.number_of_sales,
						price: item.price_cents / 100,
						published_at: item.published_at,
						updated_at: item.published_at,
						rating_point: item.rating.rating,
						rating_count: item.rating.count,
						url: item.url + '?ref=bangdc',
						trending: item.trending,
						classification: item.classification,
						classification_url: item.classification_url + '?ref=bangdc',
						thumbnail_preview: thumbnail_preview,
						tags: item.tags
					});
				});

				res.status(200).json({
					items: items,
					total_items: parsedResult.total_hits
				});
			} catch (e) {
				console.log(e);
				res.status(500).json({
					items: [],
					total_items: 0
				});
			}
		});
	}
	https.get(options, callback).end();
});

server.post('/someupdate', function(req, res) {

});

server.post('/somecreate', function(req, res) {

});

server.delete('/somedelete', function(req, res) {

});

server.listen(server.get('port'), function() {
	console.log('dttb-nodejs-api-proxy is running');
});